package com.gssc.timemanagement.controller;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {
    private final static Logger LOGGER = LogManager.getLogger(HomeController.class);

    @RequestMapping("/")
    public String home(){
        LOGGER.info("info message");
        LOGGER.debug("debug message");;
        return "This is hot and heppening!";
    }

    @RequestMapping("/test")
    public String test(){
        LOGGER.info("info message");
        LOGGER.debug("debug message");;
        return "This is Test!4";
    }


}
