import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material.module';
import { FormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';

import { NavComponent } from './core/nav/nav.component';
import { SidenavComponent } from './core/sidenav/Sidenav.component';
import { FooterComponent } from './core/footer/footer.component';
import { AuthComponent } from './components/auth/auth.component';
import { HomeComponent } from './components/home/home.component';
import { TimesheetComponent } from './components/timesheet/timesheet.component';
import { NavbarService } from './providers/services/navbar.service';
import { SidebarService } from './providers/services/sidebar.service';


@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    SidenavComponent,
    FooterComponent,
    AuthComponent,
    HomeComponent,
    TimesheetComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,

    // Flex-layout
    FlexLayoutModule
  ],
  exports: [
    NavComponent,
    SidenavComponent,
    FooterComponent
  ],
  providers: [NavbarService,
    SidebarService],
  bootstrap: [AppComponent]
})
export class AppModule { }
