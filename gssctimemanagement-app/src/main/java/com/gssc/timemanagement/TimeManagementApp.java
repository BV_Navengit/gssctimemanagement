package com.gssc.timemanagement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TimeManagementApp 
{
    public static void main( String[] args )
    {
    	SpringApplication.run(TimeManagementApp.class, args);
    }
}
